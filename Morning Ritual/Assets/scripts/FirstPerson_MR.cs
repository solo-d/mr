﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CharacterController))]
public class FirstPerson_MR : MonoBehaviour {

	public float speedMultiplier = 3.0f;
	public float mouseSensitivity = 4.0f;

	private float verticalRot = 0;

	public float UD_Range = 60.0f;

	private CharacterController FirstPerson;

	// Use this for initialization
	void Start () {
		//Screen.lockCursor = true;
		FirstPerson = GetComponent<CharacterController>();
	}
	
	// Update is called once per frame
	void Update () {
		#region rotational movements

		float rotLR = Input.GetAxis ("Mouse X") * mouseSensitivity;
		float rotUD = Input.GetAxis ("Mouse Y") * mouseSensitivity;

		verticalRot -= rotUD;
		verticalRot = Mathf.Clamp (verticalRot, -UD_Range, UD_Range);
		Camera.main.transform.localRotation = Quaternion.Euler(verticalRot,-86,0);

		#endregion

		#region keyMovements

		transform.Rotate (0, rotLR, 0);

		float forwardSpeed = Input.GetAxis ("Vertical")*-speedMultiplier;
		float sideSpeed = Input.GetAxis ("Horizontal")*-speedMultiplier;

		Vector3 speed = new Vector3 (sideSpeed, 0, forwardSpeed);

		speed = transform.rotation * speed;

		#endregion

		FirstPerson.SimpleMove (speed);
	}
}
